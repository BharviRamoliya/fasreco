import React from 'react';
import PropTypes from 'prop-types';
import {
    ScrollView, View, Image, FlatList, TouchableOpacity
} from 'react-native';
import style from '../styles/sideMenuStyle';
import TextComponent from '../common/TextComponent';

let drawerMenuData = [
    {
        icon: require('../assets/icon/user.png'),
        label: 'Add User',
        navigateOn: 'SocietyMembers'
    },
    {
        icon: require('../assets/icon/group.png'),
        label: 'All Users',
        navigateOn: 'Event'
    },
    {
        icon: require('../assets/icon/news.png'),
        label: "Today's Attendance"
    },
    {
        icon: require('../assets/icon/user.png'),
        label: "Reports"
    }
]



export default function Menu({ onItemSelected }) {
    return (
        <ScrollView scrollsToTop={ false } style={ style.menu } contentContainerStyle={ style.container }>
            <View style={ style.profileView }>
                <Image source={ require('../assets/img/profile.png') } style={ style.profileImg } />
            </View>
            <TextComponent style={ style.profileText }>Think Tanker</TextComponent>
            <FlatList
                style={ style.flatlistStyle }
                data={ drawerMenuData }
                renderItem={ ({ item }) => {
                    return (
                        <TouchableOpacity
                            activeOpacity={ 0.7 }
                        // onPress={ () => this.props.navigation.navigate(item.navigateOn) }
                        >
                            <View style={ style.listingView }>
                                <View style={ style.itemView }>
                                    <Image source={ item.icon } style={ style.itemImg } />
                                    <TextComponent style={ style.itemText }>{ item.label }</TextComponent>
                                </View>
                            </View>
                        </TouchableOpacity>
                    );
                } }
            />
            {/* <View style={ style.listingView }>
                <View style={ style.itemView }>
                    <Image source={ require('../assets/icon/user.png') } style={ style.itemImg } />
                    <TextComponent style={ style.itemText }>Add User</TextComponent>
                </View>
            </View> */}

        </ScrollView>
    );
}

Menu.propTypes = {
    onItemSelected: PropTypes.func.isRequired,
};