import React, { Component, Fragment } from 'react';
import { View, SafeAreaView, StatusBar, Image, TextInput, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import style from '../styles/LoginStyle';
import defaultStyle from '../styles/layoutStyle';
import TextComponent from '../common/TextComponent';
import { BACKGROUND_COLOR, BAR_STYLE } from '../styles/colors';

export default class Login extends Component {
    render() {
        return (
            <Fragment>
                <SafeAreaView style={ defaultStyle.topArea } />
                <SafeAreaView style={ [defaultStyle.bottomArea, { backgroundColor: 'white' }] } >
                    <StatusBar backgroundColor={ BACKGROUND_COLOR } barStyle={ BAR_STYLE } />
                    <View style={ style.container }>
                        <View style={ style.backView }>
                            <View style={ style.thirdCircle }>
                                <View style={ style.secondCircle }>
                                    <View style={ style.firstCircle }>
                                        <Image
                                            style={ style.imgStyle }
                                            source={ require('../assets/gif/userScan.gif') } />
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={ style.mainView }>
                            <View style={ style.cardView }>
                                {/* <TextComponent style={ style.errTextStyle }>please enetr password</TextComponent> */ }
                                <View style={ style.InputView }>
                                    <Image source={ require('../assets/icon/user.png') } style={ style.iconStyle } />
                                    <TextInput
                                        allowFontScaling={ false }
                                        style={ style.textInput }
                                        placeholder="Enter Username"
                                        placeholderTextColor={ BACKGROUND_COLOR }
                                        placeholderTextSize="15"
                                        keyboardType="default"
                                        // editable={ !this.props.isLoading }
                                        onChangeText={ text => this.setState({ UserName: text }) }
                                    // returnKeyType="search"
                                    // onSubmitEditing={this.props.onSubmitEditing}
                                    />
                                </View>
                                {/* <TextComponent style={ style.errTextStyle }>please enetr password</TextComponent> */ }
                                <View style={ [style.InputView, { marginBottom: 20 }] }>
                                    <Image source={ require('../assets/icon/key.png') } style={ style.iconStyle } />
                                    <TextInput
                                        allowFontScaling={ false }
                                        style={ style.textInput }
                                        placeholder="Enter Password"
                                        placeholderTextColor={ BACKGROUND_COLOR }
                                        placeholderTextSize="15"
                                        keyboardType="default"
                                        secureTextEntry={ true }
                                        // editable={ !this.props.isLoading }
                                        onChangeText={ text => this.setState({ password: text }) }
                                    // returnKeyType="search"
                                    // onSubmitEditing={this.props.onSubmitEditing}
                                    />
                                </View>
                                <TouchableOpacity style={ style.loginBtn }>
                                    <TextComponent style={ style.loginText }>Login</TextComponent>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={ style.bottomView }>
                            <TextComponent style={ [style.loginText, style.bottomText] }>Forgot Password ?</TextComponent>
                        </View>
                    </View>
                </SafeAreaView>
            </Fragment >
        )
    }
}
