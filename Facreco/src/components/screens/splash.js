import React, { Component, Fragment } from 'react';
import { View, SafeAreaView, StatusBar, Image, Text } from 'react-native';
import style from '../styles/splashStyle';
import defaultStyle from '../styles/layoutStyle';
import TextComponent from '../common/TextComponent';
import { BACKGROUND_COLOR, BAR_STYLE } from '../styles/colors';

export default class splash extends Component {
  constructor (props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate('Login');
    }, 5000);
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={ defaultStyle.topArea } />
        <SafeAreaView style={ defaultStyle.bottomArea } >
          <StatusBar backgroundColor={ BACKGROUND_COLOR } barStyle={ BAR_STYLE } />
          <View style={ style.container }>
            <View style={ style.thirdCircle }>
              <View style={ style.secondCircle }>
                <View style={ style.firstCircle }>
                  <Image
                    style={ style.imgStyle }
                    source={ require('../assets/gif/userScan.gif') } />
                </View>
              </View>
            </View>
          </View>
          <View style={ style.bottomView }>
            <TextComponent style={ style.text }>FASRECO</TextComponent>
          </View>
        </SafeAreaView>
      </Fragment>
    );
  }
}


