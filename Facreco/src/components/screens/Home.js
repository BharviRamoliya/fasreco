import React, { Component, Fragment } from 'react';
import { View, SafeAreaView, StatusBar, Image, TextInput, TouchableOpacity, KeyboardAvoidingView, StyleSheet, Text } from 'react-native';
import style from '../styles/HomeStyle';
import defaultStyle from '../styles/layoutStyle';
import TextComponent from '../common/TextComponent';
import { BACKGROUND_COLOR, BAR_STYLE } from '../styles/colors';
import SideMenu from 'react-native-side-menu';
import Menu from './Menu';
import Header from '../common/Header';



export default class Home extends Component {
    constructor (props) {
        super(props);
        this.toggle = this.toggle.bind(this);

        this.state = {
            isOpen: false,
            selectedItem: 'About',
        };
        this.state = {
        };
    }

    toggle() {
        console.log('toggle')
        this.setState({
            isOpen: !this.state.isOpen,
        });
    }

    updateMenuState(isOpen) {
        this.setState({ isOpen });
    }

    onMenuItemSelected = item =>
        this.setState({
            isOpen: false,
            selectedItem: item,
        });
    onBackPress() {
        this.props.navigation.goback();
    }


    render() {
        const menu = <Menu onItemSelected={ this.onMenuItemSelected } />;
        return (
            <Fragment>
                <SafeAreaView style={ defaultStyle.topArea } />
                <SafeAreaView style={ [defaultStyle.bottomArea, { backgroundColor: 'white', }] } >
                    <StatusBar backgroundColor={ BACKGROUND_COLOR } barStyle={ BAR_STYLE } />
                    <SideMenu
                        menu={ menu }
                        isOpen={ this.state.isOpen }
                        openMenuOffset={ 240 }
                        onChange={ isOpen => this.updateMenuState(isOpen) }
                    >
                        <Header
                            headerText={ "Home" }
                            leftImage={ (!this.state.isOpen) ? require("../assets/icon/open-menu.png") : require('../assets/icon/close.png') }
                            onpressLeft={ () => { this.toggle() } }
                        />
                        <View style={ (!this.state.isOpen) ? style.container : style.containerSide }>
                            <View style={ style.cardView }>
                                <TouchableOpacity style={ style.btn } >
                                    <TextComponent style={ style.btnText }>Check In</TextComponent>
                                </TouchableOpacity>
                                <TouchableOpacity style={ style.btn } >
                                    <TextComponent style={ style.btnText }>Check Out</TextComponent>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </SideMenu>
                </SafeAreaView>
            </Fragment>
        );
    }
}

