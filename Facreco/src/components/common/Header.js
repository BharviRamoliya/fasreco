import React, { Component } from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import style from '../styles/headerStyle';
import TextComponent from '../common/TextComponent';


export default class Header extends Component {
    constructor (props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={ style.container }>
                <TouchableOpacity
                    onPress={ this.props.onpressLeft }
                    style={ style.imgView }
                >
                    <Image
                        style={ [style.imageStyle, { marginLeft: 0, marginRight: 10 }] }
                        source={ this.props.leftImage }
                    />
                </TouchableOpacity>
                {/* : null } */ }
                <TextComponent style={ style.textStyle } >{ this.props.headerText }</TextComponent>
                {/* {(this.props.rightImage) ?     */ }
                <TouchableOpacity
                    onPress={ this.props.onRightPress }
                    style={ style.imgView }
                >
                    <Image
                        style={ style.imageStyle }
                        source={ this.props.rightImage }
                    />
                </TouchableOpacity>
            </View>
        );
    }
}
