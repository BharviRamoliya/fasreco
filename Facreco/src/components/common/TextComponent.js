import React, { PureComponent } from 'react';
import { View, Text } from 'react-native';

export default class TextComponent extends PureComponent {
    constructor (props) {
        super(props);
        this.state = {
        };
    }

    render() {
        let { style, onPress, numberOfLines } = this.props;
        return (
            <Text
                allowFontScaling={ false }
                style={ style }
                onPress={ onPress }
                numberOfLines={ numberOfLines }
            >
                { this.props.children }
            </Text>
        );
    }
}
