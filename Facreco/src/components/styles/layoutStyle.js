import { StyleSheet } from 'react-native';
import { BACKGROUND_COLOR } from './colors';

export default StyleSheet.create({
    topArea: {
        flex: 0,
        backgroundColor: BACKGROUND_COLOR
    },
    bottomArea: {
        flex: 1,
        backgroundColor: BACKGROUND_COLOR
    }
})