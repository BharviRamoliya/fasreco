//statusBar style
export const BAR_STYLE = 'light-content';

export const BACKGROUND_COLOR = '#0074F6';
export const TEXT_COLOR_SPLASH = 'white'

export const ERROR_COLOR = '#D03921';