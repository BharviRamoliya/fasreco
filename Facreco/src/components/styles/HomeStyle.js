import { StyleSheet } from 'react-native';

import { TEXT_COLOR_SPLASH, BACKGROUND_COLOR, ERROR_COLOR } from './colors';


export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        // elevation: 10
    },
    containerSide: {
        height: '85%',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        elevation: 5
    },
    cardView: {
        // backgroundColor: 'red',
        width: '100%',
        padding: 20,
        borderRadius: 5,
        bottom: 40,
        position: 'absolute',
    },
    btn: {
        padding: 10,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: BACKGROUND_COLOR,
        marginVertical: 5
    },
    btnText: {
        color: TEXT_COLOR_SPLASH,
        fontSize: 18
    }
})