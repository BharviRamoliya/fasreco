import { StyleSheet, Dimensions } from 'react-native';
import { TEXT_COLOR_SPLASH, BACKGROUND_COLOR, ERROR_COLOR } from './colors';

const window = Dimensions.get('window');

export default StyleSheet.create({
    menu: {
        flex: 1,
        width: window.width,
        height: window.height,
        backgroundColor: BACKGROUND_COLOR,
        padding: 20,
    },
    container: {
        width: 200,
        marginTop: 25,
        flex: 1,
        alignItems: 'center'
    },
    profileView: {
        height: 100,
        width: 100,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    profileImg: {
        height: 95,
        width: 95,
        borderRadius: 95 / 2
    },
    profileText: {
        color: TEXT_COLOR_SPLASH,
        fontSize: 24,
        fontWeight: '500',
        marginVertical: 10
    },
    flatlistStyle: {
        flex: 1,
        height: '100%',
        width: '100%',
        paddingTop: 40
    },
    listingView: {
        flex: 1,
        width: '100%',
    },
    itemView: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        paddingVertical: 10,
        alignItems: 'center',
        // backgroundColor: 'gray'
    },
    itemImg: {
        height: 22,
        width: 22,
        tintColor: 'white',
        marginRight: 14
    },
    itemText: {
        color: TEXT_COLOR_SPLASH,
        fontSize: 18,
        fontWeight: '300'
    }
})