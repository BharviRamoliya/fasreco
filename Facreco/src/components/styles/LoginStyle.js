import { StyleSheet } from 'react-native';
import { TEXT_COLOR_SPLASH, BACKGROUND_COLOR, ERROR_COLOR } from './colors';


export default StyleSheet.create({
    container: {
        flex: 1,
    },
    backView: {
        height: '50%',
        width: '100%',
        alignItems: 'center',
        paddingTop: 40,
        // justifyContent: 'center',
        // borderBottomRightRadius: 200,
        // borderBottomLeftRadius: 200,
        // top: -80,
        backgroundColor: BACKGROUND_COLOR
    },
    firstCircle: {
        height: 110,
        width: 110,
        borderRadius: 110 / 2,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    secondCircle: {
        height: 130,
        width: 130,
        borderRadius: 130 / 2,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255,255,255,0.5)'
    },
    thirdCircle: {
        height: 150,
        width: 150,
        borderRadius: 150 / 2,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255,255,255,0.3)',
    },
    text: {
        color: TEXT_COLOR_SPLASH,
        textAlign: 'center',
        fontSize: 22
    },
    bottomView: {
        width: '100%',
        bottom: 20
    },
    imgStyle: {
        height: 120,
        width: 120
    },
    mainView: {
        flex: 1,
        padding: 20,
        // backgroundColor: BACKGROUND_COLOR
    },
    cardView: {
        // flex: 1,
        top: '-34%',
        backgroundColor: 'white',
        padding: 15,
        borderRadius: 8,
        elevation: 5
    },
    textInput: {
        flex: 1,
        fontSize: 15,
        color: BACKGROUND_COLOR
        // backgroundColor: 'red'
    },
    InputView: {
        marginBottom: 10,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: BACKGROUND_COLOR
    },
    iconStyle: {
        height: 25,
        width: 25,
        tintColor: BACKGROUND_COLOR,
        alignSelf: 'center',
        marginRight: 15
    },
    loginBtn: {
        width: '40%',
        padding: 10,
        borderRadius: 20,
        backgroundColor: BACKGROUND_COLOR,
        alignSelf: 'center',
        position: 'absolute',
        bottom: '-12%'
    },
    loginText: {
        textAlign: 'center',
        color: 'white',
        fontSize: 15
    },
    bottomView: {
        bottom: 25,
        width: '100%',
        alignItems: 'center'
    },
    bottomText: {
        color: BACKGROUND_COLOR,
        borderBottomWidth: 1,
        borderBottomColor: BACKGROUND_COLOR,
    },
    errTextStyle: {
        color: ERROR_COLOR,
        fontSize: 12,
    }
})