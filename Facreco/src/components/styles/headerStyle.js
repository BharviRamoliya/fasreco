import { StyleSheet, Dimensions } from 'react-native';
import { TEXT_COLOR_SPLASH, BACKGROUND_COLOR, ERROR_COLOR } from './colors';

export default StyleSheet.create({
    container: {
        height: 50,
        width: '100%',
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingHorizontal: 10,
        alignItems: 'center',
        backgroundColor: BACKGROUND_COLOR
    },
    imageStyle: {
        height: 25,
        width: 25,
        tintColor: 'white',
        alignSelf: 'center',
        marginLeft: 10
    },
    imgView: {
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    startView: {
        flex: 1,
        width: '70%',
        alignSelf: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row'
    },
    textStyle: {
        fontSize: 20,
        color: 'white',
        marginLeft: 10
    }
})