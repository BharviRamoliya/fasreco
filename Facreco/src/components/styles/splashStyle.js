import { StyleSheet } from 'react-native';
import { TEXT_COLOR_SPLASH } from './colors';


export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    firstCircle: {
        height: 150,
        width: 150,
        borderRadius: 150 / 2,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    secondCircle: {
        height: 170,
        width: 170,
        borderRadius: 170 / 2,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255,255,255,0.5)'
    },
    thirdCircle: {
        height: 190,
        width: 190,
        borderRadius: 190 / 2,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255,255,255,0.3)',
    },
    text: {
        color: TEXT_COLOR_SPLASH,
        textAlign: 'center',
        fontSize: 22
    },
    bottomView: {
        width: '100%',
        bottom: 20
    },
    imgStyle: {
        height: 180,
        width: 180
    }
})