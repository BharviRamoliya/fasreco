import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

//import all screens
import splash from './components/screens/splash';
import Login from './components/screens/Login';
import Home from './components/screens/Home';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={ 'Home' }
        screenOptions={ { headerShown: false } } >
        <Stack.Screen name="splash" component={ splash } />
        <Stack.Screen name="Login" component={ Login } />
        <Stack.Screen name="Home" component={ Home } />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default App;
